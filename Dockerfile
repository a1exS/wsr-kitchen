FROM gradle:jdk11 AS builder
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle shadowJar

FROM openjdk:jdk-slim
EXPOSE 8080
RUN mkdir -p /deployments
COPY --from=builder /home/gradle/src/build/libs/*.jar /deployments/server.jar
CMD ["java", "-jar", "/deployments/server.jar"]