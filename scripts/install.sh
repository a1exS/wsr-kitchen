. ./config
cd ..

# build backend
docker build -t wsrkitchen:latest .

#run mongo
docker run -d --name mongo \
      --restart unless-stopped \
      -p $MONGO_PORT:27017 \
      -e MONGO_INITDB_ROOT_USERNAME=$MONGO_USERNAME \
      -e MONGO_INITDB_ROOT_PASSWORD=$MONGO_PASSWORD \
      mongo

# run backend
docker run -d --name wsrkitchen \
      --restart unless-stopped \
      -p $PORT:8080 \
      -e MONGO_URL="mongodb://$MONGO_USERNAME:$MONGO_PASSWORD@$IP:$MONGO_PORT" \
      -e DATABASE_NAME="wsr" \
      -e BASE_URL="http://$IP:$PORT" \
      -e SALT="wsr" \
      wsrkitchen