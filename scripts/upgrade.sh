. ./config
cd ..

# build backend
docker build -t wsrkitchen:latest .

docker stop wsrkitchen
docker rm wsrkitchen

# run backend
docker run -d --name wsrkitchen \
      --restart unless-stopped \
      -p $PORT:8080 \
      -e MONGO_URL="mongodb://$MONGO_USERNAME:$MONGO_PASSWORD@$IP:$MONGO_PORT" \
      -e DATABASE_NAME="wsr" \
      -e BASE_URL="http://$IP:$PORT" \
      -e SALT="wsr" \
      wsrkitchen