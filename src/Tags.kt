package ru.wsr

import com.papsign.ktor.openapigen.APITag

enum class Tags(override val description: String) : APITag {
    USERS("Пользователи"),
    MEALS("Блюда"),
    ORDERS("Заказы"),
    MEDIA("Медиа")
}
