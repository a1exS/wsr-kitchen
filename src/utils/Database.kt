package ru.wsr.utils

import com.mongodb.client.MongoCollection
import com.mongodb.client.result.DeleteResult
import org.litote.kmongo.*
import ru.wsr.objects.MongoDocument

object Database {

    private val client = KMongo.createClient(Config.MONGO_URL)
    val database = client.getDatabase(Config.DATABASE_NAME)

    inline fun <reified T: MongoDocument> getCol(): MongoCollection<T> =database.getCollection()

    inline fun <reified T: MongoDocument> save(obj: T):T {
        val col =  getCol<T>()
        if (obj.id == null)
            return obj.apply { col.insertOne(obj) }
        else
            if (col.updateOneById(obj.id!!, obj).matchedCount == 0L)
                return obj.apply { col.insertOne(obj) }
        return obj
    }

    inline fun <reified T: MongoDocument> saveAs(obj: T):T{
        val col =  getCol<T>()
        obj.id=null
        return obj.apply { col.insertOne(obj) }
    }

    inline fun <reified T: MongoDocument> removeById(id: String): DeleteResult {
        return getCol<T>().deleteOneById(id)
    }

    inline fun <reified T: MongoDocument> remove(obj: T): DeleteResult {
        return removeById<T>(obj.id!!)
    }

    inline fun <reified T: MongoDocument> findById(id: String?):T?{
        return id?.let { getCol<T>().findOneById(it) }
    }

}