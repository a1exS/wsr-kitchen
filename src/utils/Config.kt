package ru.wsr.utils

import java.io.File
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KVisibility
import kotlin.reflect.full.isSubtypeOf
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.starProjectedType

object Config{

    var MONGO_URL = "mongodb://wsrmongo:wsrpass@192.168.1.78"
    var DATABASE_NAME = "wsr"
    var BASE_URL = "https://1001d3109598.ngrok.io/"//"http://localhost:8080/"
    var DATA_DIR = "data"
    var SALT = "wsr"


    init {
        val env = System.getenv()
        this::class.memberProperties
            .filter{ it.visibility == KVisibility.PUBLIC }
            .filter{ it.returnType.isSubtypeOf(String::class.starProjectedType) }
            .filterIsInstance<KMutableProperty<*>>()
            .forEach { prop ->
                if (env.containsKey(prop.name))
                    prop.setter.call(this, env[prop.name])
            }
        if (!isValid()) error("BAD ENV CONFIG")
        if(!File(DATA_DIR).exists()) File(DATA_DIR).mkdirs()
    }

    fun isValid(): Boolean = MONGO_URL.isNotEmpty() && DATABASE_NAME.isNotEmpty()

}