package ru.wsr.auth

import ru.wsr.objects.User
import ru.wsr.utils.Config
import ru.wsr.utils.Database
import java.math.BigInteger
import java.security.MessageDigest

open class AuthToken (
    val userID: String,
    val passHash: String,
    val hash: String
){

    constructor(token: String): this(
        token.substringBefore("."),
        token.substringAfter(".").substringBefore("."),
        token.substringAfterLast(".")
    )

    constructor(userID: String,passHash: String): this(
        userID,passHash,"$userID ${Config.SALT} $passHash".substring(0,5)
    )

    constructor(user: User): this(
        user.id?: throw AuthenticationException("User id is null"),
        user.passHash?.md5()?.substring(0,4)?: throw AuthenticationException("Password id is null"),
    )


    open fun genHash(): String{
        return "$userID ${Config.SALT} $passHash".substring(0,5)
    }

    open fun validate(){if (hash!=genHash()) throw AuthenticationException()}

    init {
        validate()
    }

    val user by lazy { Database.findById<User>(userID)?.also{ user->
        if (passHash!=user.passHash?.md5()?.substring(0,4)?: throw AuthenticationException("Password id is null"))
            throw AuthenticationException("Password has changed")
    }?: throw AuthenticationException("User not found") }

    override fun toString(): String {
        return "${userID}.${passHash}.${hash}"
    }

}

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}