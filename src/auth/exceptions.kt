package ru.wsr.auth

class AuthenticationException(
    val text: String? = null
) : RuntimeException()
