package ru.wsr.routes

import com.google.gson.annotations.Expose
import com.papsign.ktor.openapigen.annotations.type.number.integer.min.Min
import com.papsign.ktor.openapigen.route.info
import com.papsign.ktor.openapigen.route.path.normal.NormalOpenAPIRoute
import com.papsign.ktor.openapigen.route.path.normal.get
import com.papsign.ktor.openapigen.route.path.normal.post
import com.papsign.ktor.openapigen.route.path.normal.put
import com.papsign.ktor.openapigen.route.response.respond
import com.papsign.ktor.openapigen.route.route
import com.papsign.ktor.openapigen.route.tag
import io.ktor.features.*
import jdk.jfr.Description
import org.litote.kmongo.*
import ru.wsr.Tags
import ru.wsr.objects.Meal
import ru.wsr.objects.Order
import ru.wsr.utils.Database

data class MarkReadyRequest(
    @Expose
    val orderID: String
    )

data class CreateOrderRequest(
    @Expose
    val mealData: List<MealData>,
    @Min(0)
    @Expose
    val price: Int
) {
    data class MealData(
        @Description("ID блюда")
        @Expose
        val mealID: String,
        @Description("Количество")
        @Min(1, "Количество должно быть больше нуля")
        @Expose
        val count: Int
    )
}

fun NormalOpenAPIRoute.orders() = tag(Tags.ORDERS){route("order"){

    post<AuthParams, Order, CreateOrderRequest>(
        info("Создание заказа", "Создает заказ")
    ){ authParams, request ->
        val token = authParams.token()
        respond(Database.save(Order().apply {
            userId = token.userID
            address = token.user.address
            mealIds = request.mealData.map { data -> Array(data.count){data.mealID}.toList() }.flatten()
            price = request.price
        }))
    }

    route("history"){
        get<AuthParams, List<Order>>(
            info(
               "История заказов",
                "Получение итории заказов пользователя"
            ),
        ){ auth ->
            respond(Database.getCol<Order>().find(Order::userId eq auth.token().userID).toList())
        }
    }

    route("incomplete"){
        get<AuthParams, List<Order>>(
            info(
                "Незавершенные заказы",
                "Получение активных заказов ресторана"
            ),
        ){ auth ->
            auth.token()
            respond(Database.getCol<Order>().find(Order::status `in` listOf(Order.Status.COOK, Order.Status.READY)).toList())
        }
    }

    route("markReady"){
        put<AuthParams,Any,MarkReadyRequest>(
            info("Заказ готов", "Помечает заказ как проготовленный к отправке")
        ){authParams, request ->
            authParams.token()
            val order = Database.findById<Order>(request.orderID)?: throw NotFoundException()
            if (order.status != Order.Status.COOK) throw BadRequestException("Order status is not cooking")
            Database.save(order.apply {
                updateStepTime()
                status = Order.Status.READY
            })
            respond(order)
        }

    }
}}