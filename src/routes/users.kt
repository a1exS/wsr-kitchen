import com.google.gson.annotations.Expose
import com.papsign.ktor.openapigen.annotations.type.`object`.example.ExampleProvider
import com.papsign.ktor.openapigen.annotations.type.`object`.example.WithExample
import com.papsign.ktor.openapigen.annotations.type.string.length.MinLength
import com.papsign.ktor.openapigen.route.info
import com.papsign.ktor.openapigen.route.route
import com.papsign.ktor.openapigen.route.tag
import com.papsign.ktor.openapigen.annotations.type.string.pattern.RegularExpression
import com.papsign.ktor.openapigen.route.path.normal.*
import com.papsign.ktor.openapigen.route.response.respond
import io.ktor.features.*
import io.ktor.http.auth.*
import org.litote.kmongo.and
import org.litote.kmongo.eq
import org.litote.kmongo.findOne
import ru.wsr.Tags
import ru.wsr.auth.AuthToken
import ru.wsr.auth.AuthenticationException
import ru.wsr.auth.md5
import ru.wsr.objects.User
import ru.wsr.routes.AuthParams
import ru.wsr.routes.EmptyParams
import ru.wsr.utils.Config
import ru.wsr.utils.Database
import ru.wsr.utils.emailRegex


@WithExample
data class LoginRequest(
    @Expose
    //@RegularExpression(emailRegex, "Невалидный email")
    val email: String?,

    @Expose
    //@MinLength(6, "Минимальная длина пароля - 6 символов")
    val password: String?
) {
    companion object : ExampleProvider<LoginRequest> {
        override val example = LoginRequest("mail@mail.ru", "password")
    }

    fun verify() {
        email ?: throw BadRequestException("Email must be not null")
        password ?: throw BadRequestException("Password must be not null")
    }
}

data class TokenResponse(
    @Expose
    val token: String,
)

fun NormalOpenAPIRoute.users() = tag(Tags.USERS) {

    route("signin") {
        post<EmptyParams, TokenResponse, LoginRequest>(
            info(
                summary = "Вход",
                description = "Авторизация пользователей",
            ),
        ) { _, loginRequest ->
            loginRequest.verify()
            val user = Database.getCol<User>()
                .findOne(
                    User::email eq loginRequest.email,
                    User::passHash eq (loginRequest.password + Config.SALT).md5()
                )
                ?: throw AuthenticationException("Неверный логин или пароль")
            respond(TokenResponse(AuthToken(user).toString()))
        }
    }

    route("signup") {
        post<EmptyParams, TokenResponse, LoginRequest>(
            info(
                summary = "Регистрация",
                description = "Регистрация пользователей",
            ),
        ) { _, loginRequest ->
            loginRequest.verify()
            if (Database.getCol<User>()
                    .find(
                        and(
                            User::email eq loginRequest.email,
                            User::passHash eq (loginRequest.password + Config.SALT).md5()
                        )
                    ).count() != 0
            ) throw AuthenticationException("Пользователь уже зарегистрироан")
            val user = Database.save(User().apply {
                email = loginRequest.email
                passHash = (loginRequest.password + Config.SALT).md5()
            })
            respond(TokenResponse(AuthToken(user).toString()))
        }
    }


    route("me") {

        get<AuthParams, User>(
            info(
                summary = "Информация о пользователе",
                description = "Получение информации о пользователе"
            )

        ) { authParams ->
            respond(authParams.token().user)
        }

        put<AuthParams, Any, User>(
            info(
                summary = "Обновление информации",
                description = "Обновление информации о пользователе"
            )
        ) { authParams, newUser ->
            Database.save(authParams.token().user.apply {
                name = newUser.name ?: name
                address = newUser.address ?: address
                phone = newUser.phone ?: phone
                email = newUser.email ?: email
            })
            respond(Any())
        }

        delete<AuthParams, Any>(
            info(
                summary = "Удаление пользователя",
                description = "Удаление пользователя"
            )
        ) { authParams ->
            Database.removeById<User>(authParams.token().userID)
            respond(Any())
        }
    }
}

