package ru.wsr.routes

import com.google.gson.annotations.Expose
import com.papsign.ktor.openapigen.annotations.parameters.HeaderParam
import com.papsign.ktor.openapigen.annotations.parameters.QueryParam
import com.papsign.ktor.openapigen.annotations.type.`object`.example.ExampleProvider
import com.papsign.ktor.openapigen.annotations.type.`object`.example.WithExample
import com.papsign.ktor.openapigen.annotations.type.string.example.StringExample
import com.papsign.ktor.openapigen.route.info
import com.papsign.ktor.openapigen.route.path.normal.NormalOpenAPIRoute
import com.papsign.ktor.openapigen.route.path.normal.get
import com.papsign.ktor.openapigen.route.response.respond
import com.papsign.ktor.openapigen.route.route
import com.papsign.ktor.openapigen.route.tag
import com.thedeanda.lorem.Lorem
import com.thedeanda.lorem.LoremIpsum
import io.ktor.features.*
import org.litote.kmongo.`in`
import org.litote.kmongo.eq
import org.litote.kmongo.findOne
import ru.wsr.Tags
import ru.wsr.objects.Catigory
import ru.wsr.objects.Meal
import ru.wsr.utils.Database


class MealsParams(
    @QueryParam(
        "фильтр по ID катигории блюд"
    )
    val catigory: String?,
    @HeaderParam("Bearer Authorization")
    @StringExample("Bearer <token>")
    override val Authorization: String? = null
) : AuthParams()

class MealParams(
    @QueryParam(
        "id блюда"
    )
    val id: String,
    @HeaderParam("Bearer Authorization")
    @StringExample("Bearer <token>")
    override val Authorization: String? = null
) : AuthParams()

var lorem: Lorem = LoremIpsum.getInstance()

@WithExample
class MealInfo {
    @Expose
    val deliveryInfo = lorem.getWords(10, 20)

    @Expose
    val returnPolicy = lorem.getWords(15, 20)

    companion object : ExampleProvider<MealInfo> {
        override val example = MealInfo()
    }
}

@WithExample
class StepsInfo {
    @Expose
    val stepsCount = 1 + (Math.random() * 5).toInt()

    @Expose
    val steps = Array(stepsCount) { lorem.getWords(10, 20) }

    companion object : ExampleProvider<StepsInfo> {
        override val example = StepsInfo()
    }
}

fun NormalOpenAPIRoute.meals() = tag(Tags.MEALS) {
    route("meals") {
        get<MealsParams, List<Meal>>(
            info(
                summary = "Список блюд",
                description = "Метод для получения списка блюд"
            ),
        ) { params ->
            params.token()
            if (params.catigory != null)
                respond(Database.getCol<Meal>().find(Meal::catIds `in` listOf(params.catigory)).toList())
            else
                respond(Database.getCol<Meal>().find().toList())
        }

        route("info") {
            get<MealParams, MealInfo>(
                info(
                    "Информация о блюде"
                )
            ) { params ->
                params.token()
                if (Database.getCol<Meal>().find(Meal::id eq params.id).count() > 0)
                    respond(MealInfo())
                else
                    throw NotFoundException("Блюдо не найдено")
            }
        }
        route("steps") {
            get<MealParams, StepsInfo>(
                info("Шаги приготовления блюда")
            ) { params ->
                params.token()
                if (Database.getCol<Meal>().find(Meal::id eq params.id).count() > 0)
                    respond(StepsInfo())
                else
                    throw NotFoundException("Блюдо не найдено")
            }
        }
        route("get") {
            get<MealParams, Meal>(
                info("Получить блюдо по ID")
            ) { params ->
                params.token()
                respond(
                    Database.getCol<Meal>().findOne(Meal::id eq params.id)
                        ?: throw NotFoundException("Блюдо не найдено")
                )
            }
        }
    }

    route("cats") {
        get<AuthParams, List<Catigory>>(
            info(
                summary = "Список категрий",
                description = "Метод для получения списка категорий"
            ),
        ) { authParams ->
            authParams.token()
            respond(Database.getCol<Catigory>().find().toList())
        }
    }


}