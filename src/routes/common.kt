package ru.wsr.routes

import com.papsign.ktor.openapigen.annotations.parameters.HeaderParam
import com.papsign.ktor.openapigen.annotations.type.string.example.StringExample
import ru.wsr.auth.AuthToken
import ru.wsr.auth.AuthenticationException

class EmptyParams

open class AuthParams(
    @HeaderParam("Bearer Authorization")
    @StringExample("Bearer <token>")
    open val Authorization: String? = null
){
    fun token() = AuthToken(Authorization?.substringAfter("Bearer ")?:throw AuthenticationException("No token provided"))
}