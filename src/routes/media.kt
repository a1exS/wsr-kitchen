package ru.wsr.routes

import com.papsign.ktor.openapigen.annotations.Response
import com.papsign.ktor.openapigen.content.type.binary.BinaryResponse
import com.papsign.ktor.openapigen.content.type.multipart.FormDataRequest
import com.papsign.ktor.openapigen.content.type.multipart.NamedFileInputStream
import com.papsign.ktor.openapigen.content.type.multipart.PartEncoding
import com.papsign.ktor.openapigen.route.info
import com.papsign.ktor.openapigen.route.path.normal.NormalOpenAPIRoute
import com.papsign.ktor.openapigen.route.path.normal.get
import com.papsign.ktor.openapigen.route.path.normal.post
import com.papsign.ktor.openapigen.route.response.respond
import com.papsign.ktor.openapigen.route.route
import com.papsign.ktor.openapigen.route.tag
import io.ktor.features.*
import ru.wsr.Tags
import ru.wsr.utils.Config
import java.io.File
import java.io.InputStream

@FormDataRequest
data class ImageRequest(val pic: NamedFileInputStream)

@BinaryResponse(["image/png"])
class UserPicResponse(val stream: InputStream)

fun NormalOpenAPIRoute.media() = tag(Tags.MEDIA){
    route("userpic"){
        post<AuthParams, Any, ImageRequest>(
            info(
                "Загрузка фото профиля",
                "Загружает файл в качестве фото пользователя"
            )
        ){ params, request ->
            val token = params.token()
            val userFile = File(Config.DATA_DIR,token.userID)
            if (userFile.exists()) userFile.delete()
            userFile.createNewFile()
            val fos = userFile.outputStream()
            request.pic.transferTo(fos)
            fos.flush()
            fos.close()
            respond(Any())
        }
        get<AuthParams, UserPicResponse>(
            info("Фото профиля")
        ){ params ->
            val token = params.token()
            val userFile = File(Config.DATA_DIR,token.userID)
            if (userFile.exists())
                respond(UserPicResponse(userFile.inputStream()))
            else
                throw NotFoundException()
        }
    }
}