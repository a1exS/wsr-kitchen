package ru.wsr

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.papsign.ktor.openapigen.OpenAPIGen
import com.papsign.ktor.openapigen.annotations.type.common.ConstraintViolation
import com.papsign.ktor.openapigen.openAPIGen
import com.papsign.ktor.openapigen.route.apiRouting
import com.papsign.ktor.openapigen.schema.namer.DefaultSchemaNamer
import com.papsign.ktor.openapigen.schema.namer.SchemaNamer
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.locations.*
import io.ktor.features.*
import io.ktor.gson.*
import org.litote.kmongo.*
import org.litote.kmongo.MongoOperator.*
import ru.wsr.auth.AuthenticationException
import ru.wsr.objects.Catigory
import ru.wsr.objects.Meal
import ru.wsr.objects.Order
import ru.wsr.routes.meals
import ru.wsr.routes.media
import ru.wsr.routes.orders
import ru.wsr.utils.Config
import ru.wsr.utils.Database
import users
import java.util.logging.Level
import java.util.logging.Logger
import kotlin.reflect.KType
import kotlin.reflect.javaType
import kotlin.reflect.typeOf

@ExperimentalStdlibApi
fun main(args: Array<String>) {
    init()
    startOrderUpdater()
    io.ktor.server.netty.EngineMain.main(args)
}

@ExperimentalStdlibApi
fun init(){
    val gson by lazy { Gson() }
    val applogger = Logger.getLogger("app")
    if(Database.getCol<Catigory>().countDocuments()==0L){
        val data = Thread.currentThread().contextClassLoader?.getResourceAsStream("data/cats.json")?.readAllBytes()?.decodeToString()
        val cats: Array<Catigory> = gson.fromJson(data!!, typeOf<Array<Catigory>>().javaType)
        Database.getCol<Catigory>().insertMany(cats.toMutableList())
        applogger.info("Catigories loaded from asset")
    }
    if(Database.getCol<Meal>().countDocuments()==0L){
        val data = Thread.currentThread().contextClassLoader?.getResourceAsStream("data/meals.json")?.readAllBytes()?.decodeToString()
        val meals: Array<Meal> = gson.fromJson(data!!, typeOf<Array<Meal>>().javaType)
        Database.getCol<Meal>().insertMany(meals.toMutableList())
        applogger.info("Meals loaded from asset")
    }
}

fun startOrderUpdater() = Thread{
    while (true)try{
        Thread.sleep(10000)
        Database.getCol<Order>().find(and(
            Order::nextStepTime ne null,
            Order::nextStepTime lt System.currentTimeMillis(),
            Order::status ne Order.Status.COOK,
            Order::status ne Order.Status.COMPLETED,
        )).forEach {
            it.incStatus()
            Database.save(it)
        }
    }catch (e: Exception){
        Logger.getLogger("app").log(Level.WARNING, e.stackTrace.toString())
    }
}.start()

@Suppress("unused") // Referenced in application.conf
fun Application.module() {
    install(Locations)
    install(CallLogging){
        filter { call->
            val location = call.request.uri
            if (location.contains("swagger-ui")) return@filter false
            if (location.contains("openapi")) return@filter false
            return@filter true
        }


    }

    install(ContentNegotiation) {
        gson {
            serializeNulls()
            excludeFieldsWithoutExposeAnnotation()
        }
    }

    install(CORS){
        anyHost()
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        header(HttpHeaders.ContentType)
        header(HttpHeaders.ContentLength)
        header(HttpHeaders.Authorization)
        header(HttpHeaders.AcceptLanguage)
    }

    routing {
        install(StatusPages) {
            exception<AuthenticationException> { cause ->
                call.respond(HttpStatusCode.Unauthorized,cause.text?:"")
            }
            exception<ConstraintViolation> { cause ->
                call.respond(HttpStatusCode.BadRequest,cause.message?:"")
            }
            exception<JsonSyntaxException> { cause ->
                call.respond(HttpStatusCode.BadRequest, "Invalid json data")
            }
        }

        get("/openapi.json") {
            call.respond(application.openAPIGen.api.serialize())
        }
        get("/") {
            call.respondRedirect("/swagger-ui/index.html?url=/openapi.json", true)
        }

    }

    install(OpenAPIGen) {

        info {
            version = "0.0.1"
            title = "WSR kitchen API"
        }

        server(Config.BASE_URL) {
            description = "Test server"
        }

        replaceModule(DefaultSchemaNamer, object: SchemaNamer {
            val regex = Regex("[A-Za-z0-9_.]+")
            override fun get(type: KType): String {
                return type.toString().replace(regex) { it.value.split(".").last() }.replace(Regex(">|<|, "), "_")
            }
        })
    }

    apiRouting {
        users()
        meals()
        orders()
        media()
    }
}

