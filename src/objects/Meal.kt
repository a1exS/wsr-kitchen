package ru.wsr.objects

import com.google.gson.annotations.Expose
import com.papsign.ktor.openapigen.annotations.type.`object`.example.ExampleProvider
import com.papsign.ktor.openapigen.annotations.type.`object`.example.WithExample
import com.papsign.ktor.openapigen.annotations.properties.description.Description

@WithExample
class Meal: MongoDocument() {

    @Expose
    @Description("Название")
    var title: String? = null

    @Expose
    @Description("id катигорий")
    val catIds: MutableList<String?> = mutableListOf()

    @Expose
    @Description("url картинки")
    var imgUrl: String? = null

    @Expose
    @Description("Стоимость")
    var price: Int? = null
        get() = field?: (Math.random()*2000).toInt()



    companion object: ExampleProvider<Meal>{
        override val example = Meal().apply {
            id = "testid"
            title = "Кебаб"
            catIds.add("1")
            catIds.add("2")
            catIds.add("3")
            imgUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Food_Gechresh_Azerbaijan_03.jpg/548px-Food_Gechresh_Azerbaijan_03.jpg"
        }
    }

}