package ru.wsr.objects

import com.google.gson.annotations.Expose
import com.papsign.ktor.openapigen.annotations.properties.description.Description
import com.papsign.ktor.openapigen.annotations.type.`object`.example.ExampleProvider
import com.papsign.ktor.openapigen.annotations.type.`object`.example.WithExample
import com.papsign.ktor.openapigen.annotations.type.string.pattern.RegularExpression
import ru.wsr.utils.phoneRegex

@WithExample
class User: MongoDocument() {

    @Expose
    @Description("Ник")
    var name: String? = null

    @Expose
    @Description("Email")
    //@RegularExpression(emailRegex, "Invalid email")
    var email: String? = null

    @Expose
    @Description("Телефон в международном формате")
    @RegularExpression(phoneRegex,"Invalid phone")
    var phone: String? = null

    @Expose
    @Description("Адрес")
    var address: String? = null

    var passHash: String? = null

    companion object: ExampleProvider<User> {
         override val example = User().apply {
            id = "someid"
            name = "Гена Букинг"
            email = "gena@mail.ru"
            phone = "+79257222222"
            address = "Улица Мира, дом 6"
        }
        override val examples = listOf(example)
    }

}