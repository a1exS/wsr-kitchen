package ru.wsr.objects

import com.google.gson.annotations.Expose
import org.bson.codecs.pojo.annotations.BsonId

abstract class MongoDocument {

    @BsonId
    @Expose
    open var id: String? = null

}