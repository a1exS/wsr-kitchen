package ru.wsr.objects

import com.google.gson.annotations.Expose
import com.papsign.ktor.openapigen.annotations.type.`object`.example.ExampleProvider
import com.papsign.ktor.openapigen.annotations.type.`object`.example.WithExample
import com.papsign.ktor.openapigen.annotations.properties.description.Description

@WithExample
class Order: MongoDocument() {

    enum class Status{
        CREATED,
        COOK,
        READY,
        DELIVERY,
        COMPLETED
    }

    @Expose
    @Description("ID заказчика")
    var userId: String? = null

    @Expose
    @Description("Адрес доставки")
    var address: String? = null

    @Expose
    @Description("Статус заказа")
    var status = Status.CREATED

    @Expose
    @Description("Стоиость заказа")
    var price = 0

    @Expose
    @Description("Список id блюд")
    var mealIds = listOf<String>()

    var nextStepTime: Long? = System.currentTimeMillis()+((30+Math.random()*90)*1000).toInt()

    fun updateStepTime(){
        nextStepTime = System.currentTimeMillis()+((30+Math.random()*90)*1000).toInt()
    }

    fun incStatus(){
        val statuses = Status.values()
        val index = statuses.indexOf(status)+1
        if(index<statuses.size) {
            status = statuses[index]
            updateStepTime()
        }
        else nextStepTime = null
    }

    companion object: ExampleProvider<Order>{
        override val example = Order().apply {
            id = "42"
            userId = "5"
            address = "Москва, Улица Молостовых, 10А"
            status = Status.COOK
            mealIds = listOf("1", "2", "4")
        }
    }

}