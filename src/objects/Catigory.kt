package ru.wsr.objects


import com.google.gson.annotations.Expose
import com.papsign.ktor.openapigen.annotations.properties.description.Description
import com.papsign.ktor.openapigen.annotations.type.`object`.example.ExampleProvider
import com.papsign.ktor.openapigen.annotations.type.`object`.example.WithExample

@WithExample
class Catigory: MongoDocument() {

    @Expose
    @Description("Название категории")
    var title: String? = null

    @Expose
    @Description("Описание категории")
    var description: String? = null

    companion object: ExampleProvider<Catigory>{
        override val example = Catigory().apply {
            id = "someid"
            title = "Напитки"
            description = "Жидкая еда"
        }

    }

}